.PHONY: update install uninstall

update:
	@scripts/update.sh
install:
	@scripts/install.sh
uninstall:
	@scripts/uninstall.sh
reinstall:
	@$(MAKE) uninstall
	@$(MAKE) install
	
