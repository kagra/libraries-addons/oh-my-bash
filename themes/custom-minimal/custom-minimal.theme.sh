#! bash oh-my-bash.module

SCM_NONE_CHAR=''

SCM_GIT_SHOW_MINIMAL_INFO=false
SCM_THEME_PROMPT_PREFIX="${_omb_prompt_white}- "
SCM_THEME_PROMPT_DIRTY="${_omb_prompt_yellow} (git modified)"
SCM_THEME_PROMPT_CLEAN="${_omb_prompt_bold_green} (git up-to-date)"
SCM_THEME_PROMPT_SUFFIX=""

THEME_SHOW_CLOCK=${THEME_SHOW_CLOCK:-"true"}
THEME_CLOCK_COLOR=${THEME_CLOCK_COLOR:-"$_omb_prompt_bold_magenta"}
THEME_CLOCK_FORMAT=${THEME_CLOCK_FORMAT:-"%T"}
CLOCK_THEME_PROMPT_SUFFIX=''
CLOCK_THEME_PROMPT_PREFIX=''

OMB_PROMPT_VIRTUALENV_FORMAT="${_omb_prompt_bold_gray}(%s)${_omb_prompt_reset_color}"
OMB_PROMPT_CONDAENV_FORMAT="${_omb_prompt_bold_gray}(%s)${_omb_prompt_reset_color}"
OMB_PROMPT_SHOW_PYTHON_VENV=${OMB_PROMPT_SHOW_PYTHON_VENV:=true}

function _omb_theme_PROMPT_COMMAND() {

    # This needs to be first to save last command return code
    local RC="$?"
    if [[ ${RC} == 0 ]]; then
        ret_status=""
    else
        ret_status="${_omb_prompt_brown} ✗ (last command failed)"
    fi

    # Set return status color
    local hostname="${_omb_prompt_bold_green}\u@\h"
    local python_venv; _omb_prompt_get_python_venv
    [ ! -z "$python_venv" ] && python_venv="$_omb_prompt_white$python_venv "

    # Append new history lines to history file
    history -a

    docker=""
    if [ -f /.dockerenv ]; then
        docker="${_omb_prompt_bold_cyan}docker:"  # Cyan color for 'on docker'
    fi

    PS1="${_omb_prompt_bold_magenta}[$(clock_prompt)] ${_omb_prompt_bold_green}\u@${docker}\h${_omb_prompt_white}:${_omb_prompt_blue}\w${_omb_prompt_reset_color} \$${ret_status}${_omb_prompt_white} \n$python_venv${_omb_prompt_normal}\$ "
    LS_COLORS="di=34:fi=0:ln=36:pi=33:so=32:do=35:bd=44:cd=43:or=31:mi=01;05;37:ex=01;32"
}

_omb_util_add_prompt_command _omb_theme_PROMPT_COMMAND
