#! bash oh-my-bash.module

[ "$(uname -s)" = 'Linux' ] && alias ls="LC_COLLATE=C ls --color=auto" || alias ls='ls -G --color=auto'

alias lg='ls --group-directories-first'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias grep='grep --color=auto'
