#! bash oh-my-bash.module

if [ ! -z "$(which conda)" ]; then

    if [ ! -z "$(which brew)" ]; then # Apple computers only

        # Deactivates conda before running brew.
        # Re-activates conda if it was active upon completion.
        brew() {
            local conda_env="$CONDA_DEFAULT_ENV"
            while [ "$CONDA_SHLVL" -gt 0  ]; do
                conda deactivate
            done

            command brew $@

            local brew_status=$?
            [ -n "${conda_env:+x}" ] && conda activate "$conda_env"
            return "$brew_status"
        }
    fi

    # Function to modify LD_LIBRARY_PATH for a specific Conda environment
    libconda() {

        _conda_plus_helper
        if [[ "$1" == "activate" ]]; then
            # Check if environment name is provided
            if [[ -z "$2" ]]; then
                echo "Please provide an environment to activate (add its lib path)."
                return 1
            fi

            # If a previous environment is already loaded, remove its lib path first
            if [[ -n "$CONDA_LIB_PATH" ]]; then
                export LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed -e "s|$CONDA_LIB_PATH:||")
                unset CONDA_LIB_PATH
            fi

            # Get the environment's path using conda's info
            env_path=$(conda info --envs | grep -w "$2" | awk '{print $NF}')
            
            # Check if the environment exists
            if [[ -d "$env_path" ]]; then
                env_lib_path="$env_path/lib"

                # Add the environment's lib directory to LD_LIBRARY_PATH
                export LD_LIBRARY_PATH=$env_lib_path:$LD_LIBRARY_PATH
                export CONDA_LIB_PATH=$env_lib_path
            fi

        elif [[ "$1" == "deactivate" ]]; then
            # Check if CONDA_LIB_PATH is set
            if [[ -n "$CONDA_LIB_PATH" ]]; then
                # Remove the lib directory from LD_LIBRARY_PATH
                export LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed -e "s|$CONDA_LIB_PATH:||")
                unset CONDA_LIB_PATH
            fi
        else
            echo "Usage: libconda [activate <env> | deactivate]"
        fi
    }

    # Wrapper function that replaces the conda command with additional logic for LD_LIBRARY_PATH
    _conda_wrapper() {

        local args=("$@")  # Store all arguments into the args array

        # Check if the environment name (third argument) is missing
        if [[ "$1" == "activate" && -z "${args[1]}" ]]; then
            # If CONDA_LAST_ENV is set, use it; otherwise, default to 'base'
            if [ -z "$CONDA_LAST_ENV" ]; then
                args[1]="base"
            else
                args[1]="$CONDA_LAST_ENV"
            fi
        fi

        if [[ "${args[0]}" == "deactivate" ]]; then
            export CONDA_LAST_ENV=${CONDA_DEFAULT_ENV}
        fi

        # Call the original conda command with the modified arguments
        conda ${args[@]}

        # Handle logic for activate and deactivate
        if [[ "${args[0]}" == "activate" ]]; then
            libconda activate "${args[1]}"
        elif [[ "${args[0]}" == "deactivate" ]]; then
            libconda deactivate
        fi
    }

    # Function to create aliases for all Conda environments as 'conda+<env>'
    _conda_plus_helper() {
        # Get the list of all Conda environments
        local envs=$(conda info --envs | awk '{print $1}' | tail -n +3)

        # Create an alias for each environment
        for env in $envs; do
            alias "conda+${env}"="_conda_wrapper activate ${env}"
        done
    }

    if [ ! -z "$CONDA_SHLVL" ]; then

        _conda_plus_helper

        # Run this function to create aliases at shell startup
        alias conda+='_conda_plus_helper && _conda_wrapper activate $@'
        alias conda-='_conda_plus_helper && _conda_wrapper deactivate'
        alias envconda='_conda_plus_helper && conda env list'
    fi
fi
