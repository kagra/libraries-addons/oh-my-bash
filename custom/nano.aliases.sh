#! bash oh-my-bash.module

if [ ! -z "$(which nano)" ]; then

  alias nani=nano

  nano() {

    local args=()
    for arg; do
      if [[ $arg =~ ^([^:]+):([0-9]+)(:([0-9]+))?$ ]]; then
        args+=(" +${BASH_REMATCH[2]},${BASH_REMATCH[4]:-1} ${BASH_REMATCH[1]}")
      else
        args+=("$arg")
      fi
    done

    command nano ${args[@]}
  }
fi
