#! bash oh-my-bash.module

alias scpresume="rsync --partial --progress --rsh=ssh"
