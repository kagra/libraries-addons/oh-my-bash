#! bash oh-my-bash.module

if [ ! -z "$(which git)" ]; then

	alias git-submodule-pull="git submodule update --recursive --remote"
fi
