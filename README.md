# Oh My Bash 

This repository automates the installation process for [Oh-My-Bash](https://github.com/ohmybash/oh-my-bash), a delightful, open-source, and community-driven framework for managing your Bash configuration. 

## Installation

Just clone the repository and type `make`.

Additional availables commands are: `make uninstall`, `make reinstall`, `make update`. 

Installing will perform the following actions:
- Checks if curl is installed and installs it if not.
- Prompts the user for confirmation before proceeding with Oh My Bash installation.
- Copies extra features like libraries, themes, and aliases into the Oh My Bash installation directory.
- Prompts the user to select a theme from a list of available themes.
- Allows the user to choose a minimal version of the selected theme.
- Sets up the selected theme in the Bash configuration file (`~/.bashrc`).
- Ensures that new terminal tabs open in the same directory as the current one.

Here are some additional considerations: 

- If you already have Oh My Bash installed, the script will notify you and offer to abort the installation process.
- If you don't have curl installed, the script will install it for you before proceeding with the installation.
- The script assumes that your shell configuration file is `~/.bashrc`. If you're using a different shell or configuration file, you may need to adjust the script accordingly.

## Features overview

Among the displayed features, feel free to browse the various directories inside this repository:
- `./lib` contains some default features and add-ons such as `loop`, `git-squash-all`, `grep_replace <research> <replacement> [-R -i]` commands
- `./aliases` contains some additional aliases such as `nano`. You can open nano and go to a specific line : `nano myfile.txt:<line>:<column>` (these are optional and providing line only is fine)

## License

This script is provided under the [MIT License](LICENSE).
