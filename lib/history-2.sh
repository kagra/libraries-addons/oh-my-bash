#! bash oh-my-bash.module

export HISTSIZE=
export HISTFILESIZE=

export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export BASH_SILENCE_DEPRECATION_WARNING=1
if [[ $- == *i* ]]
then
        bind '"\e[A": history-search-backward'
        bind '"\e[B": history-search-forward'
        bind '"\e[5C": forward-word'
        bind '"\e[5D": backward-word'
        bind '"\e[1;5C": forward-word'
        bind '"\e[1;5D": backward-word'
fi

shopt -s checkwinsize
shopt -s direxpand 2> /dev/null
