#! bash oh-my-bash.module

grep_replace() {

        FILE=""
        MATCH=""
        REPLACE=""
        defaults=()
        while [[ $# -ge 1 ]]; do
        key="$1"
        shift
        case $key in
        -i)
		INPLACE="1"
        ;;

        -f)
                TYPE="f"
                FILE="$1"
        shift
        ;;

        -d)
                TYPE="d"
                FILE="$1"
                shift
        ;;

        *)
                if [[ -z "$MATCH" ]]; then MATCH="$key"
                elif [[ -z "$REPLACE" ]]; then REPLACE="$key"
                else defaults+=("$key")
                fi
        ;;
        esac
        done

        [[ -z "$MATCH" ]] && echo "$0: match is missing"
        [[ -z "$MATCH" ]] && return 1
        [[ -z "$REPLACE" ]] && echo "$0: replace is missing"
        [[ -z "$REPLACE" ]] && return 1
        [[ -z "${defaults[@]}" ]] && defaults+=("$FILE")
        [[ -z "${defaults[@]}" ]] && echo "$0: target is missing"
        [[ -z "${defaults[@]}" ]] && return 1

        READLINK="readlink -f"
        FILES=$(grep -I -H "$MATCH" ${defaults[@]} | awk '{ print substr($1, 1, length($1)-1)}' | cut -d':' -f1)
        if [ ! -z "$FILES" ]; then

                var=$($READLINK $FILES | uniq )
                [[ -z "$var" ]] && echo "$0: no match found"
                [[ -z "$var" ]] && return 1
        fi

	MATCH="${MATCH//\"/\\\"}"
	MATCH="${MATCH//\//\\/}"

	REPLACE="${REPLACE//\"/\\\"}"
	REPLACE="${REPLACE//\//\\/}"

	if [ $(uname) == "Darwin" ]; then
	        echo "$var" | xargs sed -e "s/$MATCH/$REPLACE/g" -i ''
	else
	        echo "$var" | xargs sed -e "s/$MATCH/$REPLACE/g" -i
	fi
}
