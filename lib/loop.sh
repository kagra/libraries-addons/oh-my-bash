#! bash oh-my-bash.module

loop() {

        if [ $# -eq 0 ]; then
                echo "Usage: loop <command>"
                return 1
        fi

        for dir in */; do
                echo ">>> $dir"
                (
                    cd "$dir" && eval "$@"
                )
        done
}
