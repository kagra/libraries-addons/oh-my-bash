#! bash oh-my-bash.module

git-squash-all() {

    if [ $# -eq 0 ]; then
        echo "Usage: git-squash-all <initial commit>"
        return 1
    fi

    if [ -z "$(git rev-parse --is-inside-work-tree 2> /dev/null)" ]; then
        echo "This directory is not a git repository."
        return 1
    fi

    git log --oneline --decorate --graph
    read -p "Proceed with squashing these commits? (y/n): " choice
    if [ "$choice" != "y" ]; then
        echo "Aborted."
        return 1
    fi

    local COMMITMSG="$@"
    local N=$(git rev-list --count HEAD)
    local i=$((N - 1))

    while ! git reset --soft HEAD~${i}; do
        echo "Failed to reset with HEAD~${i}, trying with HEAD~$((i - 1))"
        i=$((i - 1))
        if [ $i -le 0 ]; then
            echo "Unable to reset. No more commits to try."
            return 1
        fi
    done
    git commit --amend -m "${COMMITMSG}"
}