#!/bin/bash

[[ $_ != $0 ]] && REALPATH=`dirname $(readlink -f ${BASH_SOURCE[0]})` || REALPATH=`dirname $(readlink -f $0)`

# Check if curl is installed, and install it if not
if ! command -v curl &> /dev/null; then
    echo "curl is not installed. Installing..."
    sudo apt-get update && sudo apt-get install -y curl
fi

# Prompt user for confirmation before proceeding with Oh-My-Bash installation
if [ -d ~/.oh-my-bash ]; then
    echo -e "You already have Oh My Bash installed."
else
    omb_install=false
    read -p "This script will install Oh-My-Bash. Do you want to continue? (y/n): " omb_choice
    case "$omb_choice" in
    y|Y|yes ) echo "Installing Oh-My-Bash..."; omb_install=true;;
    n|N|no ) echo "Skipping Oh-My-Bash installation.";;
    * ) echo "Invalid choice. Skipping Oh-My-Bash installation.";;
    esac

    # Installation of Oh-My-Bash
    if [ "$omb_install" = false ]; then
        echo -e "Installation aborted."
        exit 1
    fi

    curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh | bash -s

    # Import extra features
    export OSH=$HOME/.oh-my-bash
    cp -R $REALPATH/../lib/* $OSH/lib/
    cp -R $REALPATH/../themes/* $OSH/themes/
    cp -R $REALPATH/../aliases/* $OSH/aliases/
    cp -R $REALPATH/../custom/* $OSH/custom/

    # Prompt user to select a theme
    echo ""
    echo "Select a theme from the following list (or enter 'custom' for a custom theme):"
    
    ls -ls "$OSH/themes" | awk '{print $10}' | grep -v ".md" | tr '\n' ' '
    echo -e "\n"

    read -p "Enter the theme name: [default: custom] " selected_theme
    [ -z "$selected_theme" ] && selected_theme="custom"

    if [ -d "$OSH/themes/${selected_theme}-minimal" ]; then
        read -p "A minimal version of the selected theme exists. Do you want to use it? (y/n): [default: yes] " minimal_choice
        case "$minimal_choice" in
        y|Y|yes|"" ) selected_theme="${selected_theme}-minimal";;
        n|N|no ) ;;
        * ) echo "Invalid choice. Using the standard version of the theme.";;
        esac
    fi

    # Check if the system is an Apple computer
    if [ "$(uname -s)" = "Darwin" ]; then
        # Use sed -i ''
        sed -i '' -e "s/^OSH_THEME=\"font\"$/OSH_THEME=\"$selected_theme\"/" ~/.bashrc
    else
        # Use sed -i
        sed -i -e "s/^OSH_THEME=\"font\"$/OSH_THEME=\"$selected_theme\"/" ~/.bashrc
    fi

    # Disable auto update
    sed -i -e 's/# DISABLE_AUTO_UPDATE="true"/DISABLE_AUTO_UPDATE="true"/' ~/.bashrc

    echo "Installation completed successfully. Please make open a new tab or window"
    echo ". $HOME/.bashrc" >> ~/.profile
fi
