#!/bin/bash

[[ $_ != $0 ]] && REALPATH=`dirname $(readlink -f ${BASH_SOURCE[0]})` || REALPATH=`dirname $(readlink -f $0)`

# Check if curl is installed, and install it if not
if ! command -v curl &> /dev/null; then
    echo "curl is not installed. Installing..."
    apt-get update && apt-get install -y curl
fi

# Boolean variables to track user choices
omb_install=false
omp_install=false

# Prompt user for confirmation before proceeding with Oh-My-Bash installation
if [ ! -d $HOME/.oh-my-bash ]; then
    echo -e "Oh My Bash is not installed."
else
    $HOME/.oh-my-bash/tools/uninstall.sh
fi
