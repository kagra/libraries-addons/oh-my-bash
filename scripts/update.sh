#!/bin/bash

[[ $_ != $0 ]] && REALPATH=`dirname $(readlink -f ${BASH_SOURCE[0]})` || REALPATH=`dirname $(readlink -f $0)`

# Prompt user for confirmation before proceeding with Oh-My-Bash installation
if [ -d ~/.oh-my-bash ]; then
    
    # Import extra features
    export OSH=$HOME/.oh-my-bash
    cp -R $REALPATH/../lib/* $OSH/lib/
    cp -R $REALPATH/../themes/* $OSH/themes/
    cp -R $REALPATH/../aliases/* $OSH/aliases/
    cp -R $REALPATH/../custom/* $OSH/custom/

    # Disable auto update
    sed -i -e 's/# DISABLE_AUTO_UPDATE="true"/DISABLE_AUTO_UPDATE="true"/' ~/.bashrc

    echo -e "You just updated Oh My Bash configuration. Please open a new tab or window"

else
    $REALPATH/install.sh
fi
